package recursion;

/**
 * A class made for Lab 12 of CSE 271. Contains two static method
 * that both use recursion to calculate two different values.
 * @author John Belak.
 *
 */
public class Recursion {
    
    /**
     * Given an base and an exponent value, will compute recursively
     * the value of the base to the n power. So powerN(5, 2) will be
     * 25, as 5 squared (5 x 5) is 25.
     * @param base the value that will be put to the n power.
     * @param n the power put to the base. This value is the exponent
     *        in the expression base ^ n. Can not be a negative value
     *        or the method will recursively go on forever.
     * @return the int value that is the base to the n power.
     */
    public static int powerN(int base, int n) {
        return (n == 0) ? 1 : base * powerN(base, --n);
    }
    
    /**
     * Computes the total number of blocks in a triangle made of rows of 
     * blocks. The first row has 1 block, the second row has two
     * blocks and so on.
     * @param row an int value indicating the amount of rows in the
     *        triangle altogether. Value can not take a negative value, or
     *        will go on forever recusively.
     * @return an int showing the total number of blocks in the triangle
     *         with row number of rows.
     */
    public static int triangle(int row) {
        return (row == 0) ? 0 : row + triangle(--row);
    }
    
}